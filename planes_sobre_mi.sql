-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 06-03-2018 a las 07:21:30
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `efectomela`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planes`
--

CREATE TABLE IF NOT EXISTS `planes` (
  `id` int(11) NOT NULL,
  `plan1_titulo` varchar(300) NOT NULL,
  `plan1_texto` text NOT NULL,
  `plan2_titulo` varchar(300) NOT NULL,
  `plan2_texto` text NOT NULL,
  `plan3_titulo` varchar(300) NOT NULL,
  `plan3_texto` text NOT NULL,
  `valor1` varchar(100) NOT NULL,
  `valor2` varchar(100) NOT NULL,
  `valor3` varchar(100) NOT NULL,
  `valor4` varchar(100) NOT NULL,
  `por1` varchar(100) NOT NULL,
  `por2` varchar(100) NOT NULL,
  `por3` varchar(100) NOT NULL,
  `por4` varchar(100) NOT NULL,
  `extra1` varchar(500) DEFAULT NULL,
  `extra2` varchar(500) DEFAULT NULL,
  `extra3` varchar(500) DEFAULT NULL,
  `extra4` varchar(500) DEFAULT NULL,
  `incluye1` varchar(500) NOT NULL,
  `incluye2` varchar(500) NOT NULL,
  `incluye3` varchar(300) NOT NULL,
  `incluye4` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `planes`
--

INSERT INTO `planes` (`id`, `plan1_titulo`, `plan1_texto`, `plan2_titulo`, `plan2_texto`, `plan3_titulo`, `plan3_texto`, `valor1`, `valor2`, `valor3`, `valor4`, `por1`, `por2`, `por3`, `por4`, `extra1`, `extra2`, `extra3`, `extra4`, `incluye1`, `incluye2`, `incluye3`, `incluye4`) VALUES
(1, 'RENUEVATE <br> (back to basic)', 'Este es el inicio de tu nuevo estilo de vida.\r\nSi tu objetivo es la salud, limpiar tu\r\ncuerpo, desinflamarte, eliminar liquido\r\nretenido, mejorar la apariencia y\r\n textura de tu piel, aprender a comer\r\ny reconocer el verdadero sabor de lo\r\nnatural este es el plan en donde\r\naprenderÃ¡s a conocer mas tu cuerpo\r\ny el valor nutricional de los alimentos\r\n            ', 'BODYSHAPE <br> (construye tu cuerpo deseado)', '            Si tu objetivo es crecer masa muscular,\r\n            mejorar tu figura, tonificar y definir\r\n            mÃºsculos este es el plan.\r\n            Para lograr el resultado deseado es\r\n            necesario trabajar con un menu de\r\n            alimentaciÃ³n y suplementos enfocados\r\n            al crecimiento o tonificaciÃ³n de la\r\n            masa muscular.\r\n            ', 'ANTIAGING <br> (efecto mela)', 'Mejora la calidad de vida de tu\r\n              organismo y revela la belleza que hay\r\n              dentro de ti. AlimentaciÃ³n enfocada a\r\n              retrasar el proceso de envejecimiento\r\n              y prolongar los anos de juventud y no\r\n              de vejez. Mantente siempre joven joven\r\n              y vital. <br>\r\n\r\n              Puede utilizar este plan como programa\r\n              de mantenimiento.', '$75', '$60', '$55', '$50', '1 Mes', '2 Meses de contado', '3 Meses de contado', 'Rutinas de Entrenamiento', NULL, '120,00 Total (Ahorras 30,00) ', '165,00 Total (Ahorras 60,00) ', 'Tus planes de entrenamientos quedan a medio precio (50%-) por la compra de tu plan de alimentaciÃ³n. ', 'Plan de alimentacion Plan de suplementacion Chequeo Semanal', 'Plan de alimentacion Plan de suplementacion Chequeo Semanal', 'Plan de alimentacion Plan de suplementacion Chequeo Semanal', 'Rutinas de Cardio  Rutina de pesos y ejercicios  funcionales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sobre_mi`
--

CREATE TABLE IF NOT EXISTS `sobre_mi` (
  `id` int(11) NOT NULL,
  `texto` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `texto1` text NOT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `titulo` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sobre_mi`
--

INSERT INTO `sobre_mi` (`id`, `texto`, `texto1`, `imagen`, `titulo`) VALUES
(1, '<div>A los 14 aÃ±os inicie mi carrera de modelaje en PanamÃ¡ y desde allÃ­ fui preparÃ¡ndome para<br>lograr el primero de mis sueÃ±os: Miss PanamÃ¡ &nbsp;convirtiÃ©ndome en la representante de mi</div><div>paÃ­s para Miss Mundo en 2003 a la edad de 21 aÃ±os.</div>', '<div>Mi inclinaciÃ³n por la belleza y la estÃ©tica &nbsp;inicia a los 27 aÃ±os cuando decidÃ­ estudiar</div><div>cosmetologÃ­a y dedicarme de lleno al negocio de la estÃ©tica corporal.</div><div><br>Alejada de las pasarelas y dedicada a trabajar en mi negocio, descuide mucho mi<br>estilo de vida, comÃ­a mal sobre todo con el tema de las horas. &nbsp;SubÃ­ 30 libras de peso</div><div>y en agosto del 2013 a mis 31 aÃ±os, luego de muchos intentos por mejorar mi estado</div><div>fÃ­sico, dije hasta aquÃ­!!!</div>', 'perfil1.jpg', 'AsesorÃ­a de Estado FÃ­sico y Bienestar');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `planes`
--
ALTER TABLE `planes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sobre_mi`
--
ALTER TABLE `sobre_mi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `planes`
--
ALTER TABLE `planes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `sobre_mi`
--
ALTER TABLE `sobre_mi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
