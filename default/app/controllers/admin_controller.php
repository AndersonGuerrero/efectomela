<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */
class AdminController extends AdmincustomController
{

    public function index()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin/login');
      }
      $this->blogs = (new Blogs())->find();
    }

    public function logout(){
      Auth::destroy_identity();
      // Auth::destroy_active_session();
      return Redirect::to('admin/login');
    }

    public function login()
    {
      if(Auth::is_valid()){
        return Redirect::to('admin');
      }
      View::template('login');
      if(Input::hasPost('usuarios')){
        $pwd = Input::post('usuarios.clave');
        $username = Input::post('usuarios.usuario');
        $auth = new Auth("model", "class: usuarios", "usuario: $username", "clave: $pwd");
        if ($auth->authenticate()) {
          return Redirect::to("admin");
        }else{
          Flash::error('Usuario o contraseña incorrecta!');
        }
      }
    }

    public function eliminar_blog($id){
      if(!Auth::is_valid()){
        return Redirect::to('admin/login');
      }
      if ((new Blogs)->delete((int) $id)) {
              Flash::valid('Operación exitosa');
      } else {
              Flash::error('Falló Operación');
      }
      return Redirect::to("admin/");
    }

    public function nuevo_blog()
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('blogs')){
        $this->blogs = (new Blogs(Input::post('blogs')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/blogs/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          $target_path2 = $target_path.basename($_FILES['imagen_selected2']['name']);
          if(
            move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path) and
            move_uploaded_file($_FILES['imagen_selected2']['tmp_name'], $target_path2)
          ){
            $this->blogs->img_inicio = $_FILES['imagen_selected']['name'];
            $this->blogs->img_noticia = $_FILES['imagen_selected2']['name'];
          }
        }
        if ($this->blogs->save()) {
          Flash::valid("Blog Registrado");
          return Redirect::to("admin/");
        }
      }
    }

    public function planes(){
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('planes')){
        $this->planes = (new Planes(Input::post('planes')));
        if ($this->planes->save()) {
          Flash::valid("Actualizado");
          return Redirect::to("admin/planes/");
        }else{
          Flash::valid("Error al Actualizar");
        }
      }
      $this->planes = (new Planes())->find_by_first();
    }

    public function contacto(){
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('contacto')){
        $this->contacto = (new Contacto(Input::post('contacto')));
        if ($this->contacto->save()) {
          Flash::valid("Actualizado");
          return Redirect::to("admin/contacto/");
        }else{
          Flash::valid("Error al Actualizar");
        }
      }
      $this->contacto = (new Contacto())->find_by_first();
    }

    public function videos(){
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      $this->videos = (new Videos())->find();
    }

    public function agregar_video(){
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('video')){
        $this->video = (new Videos(Input::post('video')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/videos/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(
            move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)
          ){
            $this->video->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->video->save()) {
          Flash::valid("Registrado");
          return Redirect::to("admin/videos/");
        }else{
          Flash::valid("Error al Actualizar");
        }
      }
    }

    public function editar_video($id){
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('video')){
        $this->video = (new Videos(Input::post('video')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/videos/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(
            move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)
          ){
            $this->video->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->video->update()) {
          Flash::valid("Actualizado");
          return Redirect::to("admin/videos/");
        }else{
          Flash::valid("Error al Actualizar");
        }
      }
      $this->video = (new Videos())->find_by_id((int)$id);
    }

    public function sobre_mi(){
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('sobre_mi')){
        $this->sobre_mi = (new Sobre_mi(Input::post('sobre_mi')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/sobre_mi/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(
            move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)
          ){
            $this->sobre_mi->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->sobre_mi->save()) {
          Flash::valid("Actualizado");
          return Redirect::to("admin/sobre_mi/");
        }else{
          Flash::valid("Error al Actualizar");
        }
      }
      $this->sobre_mi = (new Sobre_mi())->find_by_first();
    }

    public function editar_blog($id)
    {
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('blogs')){
        $this->blogs = (new Blogs(Input::post('blogs')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/blogs/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          $target_path2 = $target_path.basename($_FILES['imagen_selected2']['name']);
          if(
            move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path) and
            move_uploaded_file($_FILES['imagen_selected2']['tmp_name'], $target_path2)
          ){
            $this->blogs->img_inicio = $_FILES['imagen_selected']['name'];
            $this->blogs->img_noticia = $_FILES['imagen_selected2']['name'];
          }
        }
        if ($this->blogs->update()) {
          Flash::valid("Blog actualizado!");
          return Redirect::to("admin/");
        }
      }else{
        $this->blogs = (new Blogs())->find_by_id((int)$id);
      }
    }

    public function sliders(){
      $this->sliders = (new Sliders())->find();
    }

    public function nuevo_slider(){
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('sliders')){
        $this->sliders = (new Sliders(Input::post('sliders')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/sliders/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->sliders->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->sliders->save()) {
          Flash::valid("Slider Registrado");
          return Redirect::to("admin/sliders/");
        }else{
          Flash::error("Error al registrar el slider");
          return Redirect::to("admin/sliders/");
        }
      }
    }

    public function editar_slider($id){
      if(!Auth::is_valid()){
        return Redirect::to('admin');
      }
      if(Input::hasPost('sliders')){
        $this->sliders = (new Sliders(Input::post('sliders')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/sliders/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->sliders->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->sliders->update()) {
          Flash::valid("Slider actualizado!");
          return Redirect::to("admin/sliders/");
        }
      }else{
        $this->sliders = (new Sliders())->find_by_id((int)$id);
      }
    }

    public function galeria(){
      $this->galeria = (new Galeria())->find();
    }

    public function agregar_foto(){
      if(Input::hasPost('galeria')){
        $this->galeria = (new Galeria(Input::post('galeria')));
        if($_FILES){
          $target_path = FILES_PATH.'upload/galeria/';
          $target_path = $target_path.basename($_FILES['imagen_selected']['name']);
          if(move_uploaded_file($_FILES['imagen_selected']['tmp_name'], $target_path)){
            $this->galeria->imagen = $_FILES['imagen_selected']['name'];
          }
        }
        if ($this->galeria->save()) {
          Flash::valid("Foto Registrado");
          return Redirect::to("admin/galeria");
        }
      }
    }

    public function editar_foto($id){
      $this->galeria = (new Galeria())->find_by_id((int)$id);
    }

    public function eliminar_foto($id){
      (new Galeria())->delete((int)$id);
      Flash::valid("Foto eliminada!");
      return Redirect::to("admin/galeria");
    }
}
