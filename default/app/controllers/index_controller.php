<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */
class IndexController extends AppController
{

    public function index()
    {
      $this->sliders = (new Sliders())->find(
        "order: created_at asc"
      );
      $this->blogs = (new Blogs())->find(
        "limit: 3",
        "order: created_at desc"
      );
      $this->galeria = (new Galeria())->find();
      $this->sobre_mi = (new Sobre_mi())->find_by_first();
      $this->planes = (new Planes())->find_by_first();
      $this->contacto = (new Contacto())->find_by_first();
      $this->videos = (new Videos())->find(
        "order: principal"
      );
    }

    public function blog($id){
      $this->blog = (new Blogs())->find_by_id((int)$id);
      $this->blogs = (new Blogs())->find(
        "order: created_at desc",
        "conditions: id !=".$id
      );
    }

    public function blogs(){
      $this->blogs = (new Blogs())->find(
        "order: created_at desc"
      );
    }
}
