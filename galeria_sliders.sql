-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 02-03-2018 a las 20:28:50
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `efectomela`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

CREATE TABLE IF NOT EXISTS `galeria` (
  `id` int(11) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `categoria` varchar(300) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galeria`
--

INSERT INTO `galeria` (`id`, `imagen`, `categoria`, `created_at`) VALUES
(1, 'img1.jpg', 'Promoviendo Salud', '2018-03-01 04:12:40'),
(2, 'img2.jpg', 'Promoviendo Salud', '2018-03-01 04:13:13'),
(3, 'img3.jpg', 'Promoviendo Salud', '2018-03-01 04:13:37'),
(4, 'img4.jpg', 'Promoviendo Salud', '2018-03-01 04:13:54'),
(5, 'img5.jpg', 'Promoviendo Salud', '2018-03-01 04:14:15'),
(6, 'img6.jpg', 'Promoviendo Salud', '2018-03-01 04:14:34'),
(7, 'img7.jpg', 'Promoviendo Salud', '2018-03-01 04:15:13'),
(8, 'img8.jpg', 'Promoviendo Salud', '2018-03-01 04:15:33'),
(9, 'img9.jpg', 'Promoviendo Salud', '2018-03-01 04:16:21'),
(10, 'img11.jpg', 'Promoviendo Salud', '2018-03-01 04:16:43'),
(11, 'img12.jpg', 'Diva', '2018-03-01 04:18:52'),
(12, 'img13.jpg', 'Diva', '2018-03-01 04:19:21'),
(13, 'img14.jpg', 'Diva', '2018-03-01 04:19:44'),
(14, 'img15.jpg', 'Diva', '2018-03-01 04:20:54'),
(15, 'img16.jpg', 'Diva', '2018-03-01 04:21:25'),
(16, 'img18.jpg', 'Diva', '2018-03-01 04:21:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(11) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `texto` text NOT NULL,
  `texto_A` varchar(500) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `color_texto` varchar(30) NOT NULL,
  `color_texto_A` varchar(30) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sliders`
--

INSERT INTO `sliders` (`id`, `titulo`, `texto`, `texto_A`, `imagen`, `color_texto`, `color_texto_A`, `created_at`) VALUES
(1, '#EFECTOMELA', '"No siempre estarÃ¡s motivado. Tienes que aprender  a ser disciplinado."', '@efectomela', 'banner.jpg', '#000000', '#6d0c49', '2018-02-26 22:12:24'),
(2, '#EFECTOMELA', '"La competencia es contigo mismo"', '@efectomela', 'banner2.jpg', '#ffffff', '#f9c809', '2018-02-26 22:46:08'),
(3, '#EFECTOMELA', '"Hacerte consciente de tu propio trabajo es mi misiÃ³n"', '@efectomela', 'banner4.jpg', '#ffffff', '#f9c809', '2018-02-26 22:47:24');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
