-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-03-2018 a las 16:21:48
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `efectomela`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
  `id` int(11) NOT NULL,
  `web` varchar(300) NOT NULL,
  `correo` varchar(300) NOT NULL,
  `facebook` varchar(300) NOT NULL,
  `youtube` varchar(300) NOT NULL,
  `instagram` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`id`, `web`, `correo`, `facebook`, `youtube`, `instagram`) VALUES
(1, 'www.efectomela.com', 'info@efectomela.com', 'https://www.facebook.com/', 'https://www.youtube.com/channel/UCsLN8nmWRc1hqt9ZTEaLaxA', 'https://www.instagram.com/melapiedrahita/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `link` varchar(500) NOT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `principal` tinyint(1) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `titulo`, `link`, `imagen`, `principal`, `created_at`) VALUES
(1, 'Rutina GAP jelou', 'https://www.youtube.com/embed/VreKuQJxhEQ', 'video1.jpg', 1, '2018-03-06 00:00:00'),
(2, 'Power Abs desde JelouTv', 'https://www.youtube.com/embed/f24GLQMCMv4', 'video2.jpg', 0, '2018-03-07 02:30:35'),
(3, 'Proyecto Bumbumnanuca', 'https://www.youtube.com/embed/DwlTd7cFGEE', 'video3.jpg', 0, '2018-03-07 02:31:38'),
(4, 'Suplementos Efecto Mela', 'https://www.youtube.com/embed/GGI4gzPoe5Q', 'video4.jpg', 0, '2018-03-07 02:43:57');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
